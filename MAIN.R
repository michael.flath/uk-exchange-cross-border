
library(tidyverse)
library(lubridate)
library(dbplyr)
library(xlsx)
library(optimaxload)
library(optimaxutils)



# parameters --------------------------------------------------------------

start_date <- "2022-01-01"
end_date <- "2022-04-16"



# sections ----------------------------------------------------------------

source("functions.R")


source("00_trades.R")


source("10_capacities.R")


source("20_market_potential.R")


source("30_market_potential_summary.R")


source("40_results.R")
